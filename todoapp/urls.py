from django.urls import path
from . import views

urlpatterns = [
    path('', views.task, name='todo'),
    path('<int:task_id>/delete', views.delete_task, name='delete_task'),
    path('<int:task_id>', views.edit_task, name='edit_task'),
]
