from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    deadline = models.CharField(max_length=200, null=False)
    task_name = models.CharField(max_length=200, null=False)
    description = models.CharField(max_length=255, null=False)
    done = models.BooleanField(null=True, default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
