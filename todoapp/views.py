from django.shortcuts import render, redirect
from .models import Task
from .forms import CreateUserForm, TaskForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required


def register_page(request):
    if request.user.is_authenticated:
        return redirect('todo')
    else:
        form = CreateUserForm

        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)

                return redirect('login')

    context = {'form': form}
    return render(request, 'register.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('todo')
    else:

        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('todo')
            else:
                messages.info(request, 'Username OR password is incorrect')

    context = {}
    return render(request, 'login.html', context)


def logout_user(request):
    logout(request)
    return redirect('login')


@login_required(login_url='login')
def task(request):
    user = get_user(request)
    form = TaskForm()
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            Task.objects.create(
                task_name=form.cleaned_data['task_name'],
                description=form.cleaned_data['description'],
                deadline=form.cleaned_data['deadline'],
                user=user
            )
    my_tasks = Task.objects.filter(user=user)
    return render(request, 'todolist.html', {'form': form, 'tasks': my_tasks})


@login_required(login_url='login')
def edit_task(request, task_id):
    user = get_user(request)
    try:
        my_task = Task.objects.get(id=task_id)
        form = TaskForm(instance=my_task)
        if request.method == 'POST':
            form = TaskForm(request.POST, instance=my_task)
            if form.is_valid():
                form.save()
                return redirect('todo')
        if not my_task.user == user:
            messages.error(request, 'This is not your task!')
        return render(request, 'todo.html', {'form': form, 'task': my_task})
    except Task.DoesNotExist:
        messages.error(request, 'Task does not exist.')
    return redirect('todo')


@login_required(login_url='login')
def delete_task(request, task_id):
    user = get_user(request)
    try:
        my_task = Task.objects.get(id=task_id)
        if my_task.user == user:
            my_task.delete()
        else:
            messages.error(request, 'This is not your task!')
    except Task.DoesNotExist:
        messages.error(request, 'Task does not exist.')
    return redirect('todo')
