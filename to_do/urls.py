from django.contrib import admin
from django.urls import path, include
from todoapp import views as todo_views, views


urlpatterns = [
    path('register/', todo_views.register_page, name="register"),
    path('login/', todo_views.login_page, name="login"),
    path('logout/', todo_views.logout_user, name="logout"),
    path('admin/', admin.site.urls),
    path('todo/', include('todoapp.urls')),
]
